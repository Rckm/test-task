package dmitry.berdnikov.testtask;

import java.util.List;


public interface TaskHandler {
    void onGetResponse(List<JobItem> jobItems);
}
