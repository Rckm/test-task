package dmitry.berdnikov.testtask;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


public class TaskReciever extends AsyncTask<Void, Void, Void> {
    private static final String URL_REQUEST = "http://test.boloid.com:9000/tasks";
    private static final String ARRAY_TASKS_NAME_FIELD = "tasks";
    private static final String ARRAY_PRICE_NAME_FIELD = "prices";
    private static final String TITLE_FIELD = "title";
    private static final String DATE_FIELD = "date";
    private static final String TEXT_FIELD = "text";
    private static final String LONG_TEXT_FIELD = "longText";
    private static final String LOCATION_TEXT_FIELD = "locationText";
    private static final String LOCATION_OBJECT_FIELD = "location";
    private static final String LAT_COORDINAT_FIELD = "lat";
    private static final String LON_COORDINAT_FIELD = "lon";

    private final ArrayList<JobItem> mJobItems = new ArrayList<JobItem>();
    private final TaskHandler mHandler;

    public TaskReciever(TaskHandler handler) {
        mHandler = handler;
    }

    public void getJobs() {
        URL url = null;
        HttpURLConnection urlConnection = null;
        JSONArray tasks = null;

        try {
            url = new URL(URL_REQUEST);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        try {
            urlConnection = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            JSONObject dataJsonObj = new JSONObject(readStream(reader));
            tasks = dataJsonObj.getJSONArray(ARRAY_TASKS_NAME_FIELD);

            for (int i = 0; i < tasks.length(); ++i) {
                JSONObject object = tasks.getJSONObject(i);
                JSONObject objectLocation = object.getJSONObject(LOCATION_OBJECT_FIELD);
                JSONArray prices = object.getJSONArray(ARRAY_PRICE_NAME_FIELD);

                JobItem item = new JobItem(
                        object.getString(TITLE_FIELD),
                        object.getString(DATE_FIELD),
                        object.getString(TEXT_FIELD),
                        object.getString(LONG_TEXT_FIELD),
                        object.getString(LOCATION_TEXT_FIELD),
                        objectLocation.getDouble(LAT_COORDINAT_FIELD),
                        objectLocation.getDouble(LON_COORDINAT_FIELD),
                        prices);

                mJobItems.add(item);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
    }

    private String readStream(BufferedReader reader) throws IOException {
        String resultJson = "";
        final StringBuilder response = new StringBuilder();

        if (reader != null) {
            String str;
            while ((str = reader.readLine()) != null) {
                response.append(str);
            }
            resultJson = response.toString();
        } else {
            throw new IOException();
        }
        return resultJson;
    }

    @Override
    protected Void doInBackground(Void... params) {
        getJobs();
        mHandler.onGetResponse(mJobItems);
        return null;
    }
}
