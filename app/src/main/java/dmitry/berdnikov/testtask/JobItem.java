package dmitry.berdnikov.testtask;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class JobItem implements Parcelable {
    private static final String PRICE_FIELD = "price";
    private static final String DESCRIPTION_FIELD = "description";

    final String mTitle;
    final String mDate;
    final String mText;
    final String mLongText;
    final String mLocationText;
    final double mLat;
    final double mLon;

    final ArrayList<PriceItem> mPriceArray = new ArrayList<PriceItem>();


    public JobItem(String title, String date, String text, String longText,
                   String location, double lat, double lon, JSONArray prices) {
        mTitle = title;
        mDate = date;
        mText = text;
        mLongText = longText;
        mLocationText = location;
        mLat = lat;
        mLon = lon;

        for (int i = 0; i < prices.length(); ++i) {
            try {
                JSONObject object = prices.getJSONObject(i);
                mPriceArray.add(new PriceItem(
                        object.getInt(PRICE_FIELD),
                        object.getString(DESCRIPTION_FIELD)));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private JobItem(Parcel in) {
        mTitle = in.readString();
        mDate = in.readString();
        mText = in.readString();
        mLongText = in.readString();
        mLocationText = in.readString();
        mLat = in.readDouble();
        mLon = in.readDouble();
        in.readTypedList(mPriceArray, PriceItem.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mTitle);
        dest.writeString(mDate);
        dest.writeString(mText);
        dest.writeString(mLongText);
        dest.writeString(mLocationText);
        dest.writeDouble(mLat);
        dest.writeDouble(mLon);
        dest.writeTypedList(mPriceArray);
    }

    public static final Parcelable.Creator<JobItem> CREATOR = new Parcelable.Creator<JobItem>() {

        public JobItem createFromParcel(Parcel in) {
            return new JobItem(in);
        }

        public JobItem[] newArray(int size) {
            return new JobItem[size];
        }
    };

    /*************************************Inner class****************************************/

    public static class PriceItem implements Parcelable {
        final int mPrice;
        final String mDescription;

        public PriceItem(int price, String description) {
            mPrice = price;
            mDescription = description;
        }

        private PriceItem(Parcel in) {
            mPrice = in.readInt();
            mDescription = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(mPrice);
            dest.writeString(mDescription);
        }

        public static final Parcelable.Creator<PriceItem> CREATOR = new Parcelable.Creator<PriceItem>() {

            public PriceItem createFromParcel(Parcel in) {
                return new PriceItem(in);
            }

            public PriceItem[] newArray(int size) {
                return new PriceItem[size];
            }
        };
    }
}
