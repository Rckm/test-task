package dmitry.berdnikov.testtask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.OverlayManager;
import ru.yandex.yandexmapkit.overlay.Overlay;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.overlay.balloon.BalloonItem;
import ru.yandex.yandexmapkit.overlay.balloon.OnBalloonListener;
import ru.yandex.yandexmapkit.utils.GeoPoint;


public class MainActivity extends Activity implements OnBalloonListener, TaskHandler {
    private final Map<BalloonItem, JobItem> mBallonMap = new HashMap<BalloonItem, JobItem>();
    private TaskReciever mTaskReciever;
    private MapController mMapController;
    private OverlayManager mOverlayManager;
    private Button mUpdateTasksButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_layout);

        mUpdateTasksButton = (Button) findViewById(R.id.updateButton);
        mUpdateTasksButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTaskReciever = new TaskReciever(MainActivity.this);
                mTaskReciever.execute();
            }
        });

        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            initMapAndTasks();
        } else {
            final Toast toast = Toast.makeText(getApplicationContext(),
                    getString(R.string.network_error), Toast.LENGTH_LONG);
            toast.show();
        }
    }

    private void initMapAndTasks() {
        final MapView mapView = (MapView) findViewById(R.id.map);
        mapView.showBuiltInScreenButtons(true);

        mMapController = mapView.getMapController();

        mOverlayManager = mMapController.getOverlayManager();
        mOverlayManager.getMyLocation().setEnabled(false);

        mTaskReciever = new TaskReciever(this);
        mTaskReciever.execute();
    }

    @Override
    public void onGetResponse(List<JobItem> jobItems) {
        Overlay overlay = new Overlay(mMapController);
        Resources res = getResources();

        for (JobItem item: jobItems) {
            final OverlayItem overlayItem = new OverlayItem(new GeoPoint(item.mLat , item.mLon), res.getDrawable(R.drawable.a));
            final BalloonItem balloon = new BalloonItem(this, overlayItem.getGeoPoint());

            balloon.setOnBalloonListener(this);
            balloon.setText(item.mTitle);

            mBallonMap.put(balloon, item);

            overlayItem.setBalloonItem(balloon);
            overlay.addOverlayItem(overlayItem);
        }

        mOverlayManager.addOverlay(overlay);
    }

    @Override
    public void onBalloonViewClick(BalloonItem balloonItem, View view) {
        final Intent intent = new Intent().setClass(this, DescriptionJobActivity.class);
        intent.putExtra(DescriptionJobActivity.ID_DESCRIPTION, mBallonMap.get(balloonItem));

        startActivity(intent);
    }

    @Override
    public void onBalloonShow(BalloonItem balloonItem) {
    }

    @Override
    public void onBalloonHide(BalloonItem balloonItem) {
    }

    @Override
    public void onBalloonAnimationStart(BalloonItem balloonItem) {
    }

    @Override
    public void onBalloonAnimationEnd(BalloonItem balloonItem) {
    }
}
