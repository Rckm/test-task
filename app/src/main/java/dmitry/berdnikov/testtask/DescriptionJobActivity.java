package dmitry.berdnikov.testtask;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class DescriptionJobActivity extends Activity {
    public final static String ID_DESCRIPTION = "info";

    private TextView textView;
    private TextView longTextView;
    private TextView locationTextView;
    private TextView dateTextView;
    private ListView mListJobs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.description_job_fragment_layout);

        textView = (TextView) findViewById(R.id.text);
        longTextView = (TextView) findViewById(R.id.longText);
        locationTextView = (TextView) findViewById(R.id.locationText);
        dateTextView = (TextView) findViewById(R.id.dateText);

        JobItem item = getIntent().getParcelableExtra(ID_DESCRIPTION);

        textView.setText(item.mTitle);
        longTextView.setText(item.mLongText);
        locationTextView.setText(item.mLocationText);
        dateTextView.setText(item.mDate);

        mListJobs = (ListView) findViewById(R.id.listJobs);
        mListJobs.setAdapter(new SimpleAdapter(this, item.mPriceArray));
    }

    private static class SimpleAdapter extends ArrayAdapter<JobItem.PriceItem> {
        private Context mContext;
        private ArrayList<JobItem.PriceItem> mList;

        public SimpleAdapter(final Context context, ArrayList<JobItem.PriceItem> list) {
            super(context, R.layout.item_list_layout, list);
            mContext = context;
            mList = list;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = null;

            if (convertView == null) {
                LayoutInflater inflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflator.inflate(R.layout.item_list_layout, null);
            } else {
                view = convertView;
            }
            JobItem.PriceItem item = mList.get(position);

            ((TextView) view.findViewById(R.id.infoItem)).setText(String.valueOf(item.mDescription));
            ((TextView) view.findViewById(R.id.priceItem)).setText(String.valueOf(item.mPrice));

            return view;
        }
    }
}
